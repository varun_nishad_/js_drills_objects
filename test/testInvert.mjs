import { invert } from '../invert.mjs'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = invert(testObject);
console.log(result)
