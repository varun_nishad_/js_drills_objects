import { defaults } from '../default.mjs'

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps = { name: 'Bruce Wayne', age: 36, location: 'Gotham' ,color:"white"};
const result = defaults(testObject, defaultProps)

console.log(result)
