export function invert(obj) {
  for(let key in obj){
      let newKey = obj[key]
      let newVal = key
      obj[newKey] = newVal
      delete obj[key]
  }
return obj
}
