export function cbFun(key){
  return key + '-Added'
}

export function mapObject(obj, cb) {
 for(let key in obj){
     obj[key] = cbFun(obj[key])
 }
 return obj
}
